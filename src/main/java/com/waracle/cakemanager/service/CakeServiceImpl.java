package com.waracle.cakemanager.service;

import com.waracle.cakemanager.jpa.CakeEntity;
import com.waracle.cakemanager.jpa.CakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CakeServiceImpl implements CakeService {

    @Autowired
    private CakeRepository repository;

    @Override
    public List<CakeEntity> getAllCakes() {
        return new ArrayList<>(repository.findAll());
    }

    @Override
    public boolean cakeAlreadyAdded(String cakeTitle) { return !repository.findByTitle(cakeTitle).isEmpty(); }

    @Override
    public void save(CakeEntity cake) {
        repository.save(cake);
    }


}
