package com.waracle.cakemanager.service;

import com.waracle.cakemanager.jpa.CakeEntity;

import java.util.List;

interface CakeService {

    List<CakeEntity> getAllCakes();

    boolean cakeAlreadyAdded(String cakeTitle);

    void save(CakeEntity cake);

}
