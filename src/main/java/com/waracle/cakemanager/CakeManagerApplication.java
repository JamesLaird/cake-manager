package com.waracle.cakemanager;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.waracle.cakemanager.jpa.CakeEntity;
import com.waracle.cakemanager.jpa.CakeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication
public class CakeManagerApplication {

    private static final Logger logger = LoggerFactory.getLogger(CakeManagerApplication.class);
    private static final String JSON_DATA_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";

    public static void main(String[] args) {
        SpringApplication.run(CakeManagerApplication.class, args);
    }

    @Bean
    ApplicationRunner init(CakeRepository repository) {
        return args -> {
            logger.info("Starting loading cakes json");
            try (InputStream inputStream = new URL(JSON_DATA_URL).openStream()) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                List cakeTitles = new ArrayList();

                StringBuffer buffer = new StringBuffer();
                String line = reader.readLine();
                while (line != null) {
                    buffer.append(line);
                    line = reader.readLine();
                }

                logger.debug("parsing cake json");
                JsonParser parser = new JsonFactory().createParser(buffer.toString());
                if (JsonToken.START_ARRAY != parser.nextToken()) {
                    throw new Exception("bad token");
                }

                JsonToken nextToken = parser.nextToken();
                while (nextToken == JsonToken.START_OBJECT) {

                    CakeEntity cakeEntity = new CakeEntity();
                    logger.debug(parser.nextFieldName());
                    cakeEntity.setTitle(parser.nextTextValue());

                    logger.debug(parser.nextFieldName());
                    cakeEntity.setDescription(parser.nextTextValue());

                    logger.debug(parser.nextFieldName());
                    cakeEntity.setImage(parser.nextTextValue());
                    if (!cakeTitles.contains(cakeEntity.getTitle())) {
                        repository.save(cakeEntity);
                        cakeTitles.add(cakeEntity.getTitle());
                    }

                    nextToken = parser.nextToken();
                    logger.debug(String.valueOf(nextToken));

                    nextToken = parser.nextToken();
                    logger.debug(String.valueOf(nextToken));
                }

            } catch (Exception ex) {
                logger.error("Loading cake json error");
                throw new Exception(ex);
            }
            logger.info("Finished loading cakes json");
        };
    }

    @Bean
    public FilterRegistrationBean<CorsFilter> simpleCorsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        List whitelistUrls = new ArrayList();
        whitelistUrls.add("http://localhost:4200");
        whitelistUrls.add("http://localhost:3000");
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(whitelistUrls);
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }
}
