package com.waracle.cakemanager.jpa;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "Cakes", uniqueConstraints = {@UniqueConstraint(columnNames = "ID"), @UniqueConstraint(columnNames = "TITLE")})
public class CakeEntity implements Serializable {

    private static final long serialVersionUID = -1798070786993154676L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long cakeId;

    @NotBlank(message = "Cake title is required")
    @Size(max=100, message = "Cake title cannot be greater than 100 characters in length")
    @Column(name = "TITLE", unique = true, nullable = false, length = 100)
    private String title;

    @NotBlank(message = "Cake description is required")
    @Size(max=100, message = "Cake description cannot be greater than 100 characters in length")
    @Column(name = "DESCRIPTION", length = 100)
    private String description;

    @NotBlank(message = "Cake image is required")
    @Size(max=300, message = "Cake images cannot be greater than 300 characters in length")
    @Column(name = "IMAGE", nullable = false, length = 300)
    private String image;

    public Long getCakeId() { return cakeId; }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return String.format(
                "Cake[id=%d, title='%s', description='%s', image='%s']",
                getCakeId(), getTitle(), getDescription(), getImage());
    }

}