package com.waracle.cakemanager.rest;


import com.waracle.cakemanager.error.CakeErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.waracle.cakemanager.jpa.CakeEntity;
import com.waracle.cakemanager.service.CakeServiceImpl;

import java.util.Collection;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
class CakeController {

    private static final String CAKES_FILENAME = "cakes.json";

    @Autowired
    private
    CakeServiceImpl cakeService;

    @GetMapping("/getAllCakes")
    public Collection<CakeEntity> getAllCakes() {
        return cakeService.getAllCakes();
    }

    @GetMapping("/cakes")
    public ResponseEntity<byte[]> downloadCakesList() throws Exception {
        List<CakeEntity> cakes = cakeService.getAllCakes();
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(cakes);
        byte[] isr = json.getBytes();
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentLength(isr.length);
        respHeaders.setContentType(new MediaType("text", "json"));
        respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + CAKES_FILENAME);
        return new ResponseEntity<>(isr, respHeaders, HttpStatus.OK);
    }

    @PostMapping("/cakes")
    public ResponseEntity<?> addCake(@RequestBody CakeEntity cake) {

        if (cakeService.cakeAlreadyAdded(cake.getTitle())) {
            return new ResponseEntity(new CakeErrorResponse("Unable to create. A cake with title " +
                    cake.getTitle() + " already exists."), HttpStatus.CONFLICT);
        }
        cakeService.save(cake);

        return new ResponseEntity<String>(HttpStatus.CREATED);
    }
}