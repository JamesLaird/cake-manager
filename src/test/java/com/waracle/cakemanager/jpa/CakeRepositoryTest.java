package com.waracle.cakemanager.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CakeRepositoryTest {

    @Autowired
    private CakeRepository cakeRepository;

    @Test
    public void whenFindingCakeById_thenCorrect() {
        CakeEntity birthday = new CakeEntity();
        birthday.setTitle("Cake For Afternoon Tea");
        birthday.setDescription("afternoon tea cake");
        birthday.setImage("afternoon-tea.jpg");
        cakeRepository.save(birthday);
        assertThat(cakeRepository.findById(1L)).isInstanceOf(Optional.class);
    }

    @Test
    public void whenFindingAllCakes_thenCorrect() {
        CakeEntity birthday = new CakeEntity();
        birthday.setTitle("Christmas");
        birthday.setDescription("christmas cake");
        birthday.setImage("christmas.jpg");
        CakeEntity wedding = new CakeEntity();
        wedding.setTitle("Wedding");
        wedding.setDescription("wedding cake");
        wedding.setImage("wedding.jpg");
        cakeRepository.save(birthday);
        cakeRepository.save(wedding);
        assertThat(cakeRepository.findAll()).isInstanceOf(List.class);
    }
}