package com.waracle.cakemanager.rest;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.waracle.cakemanager.jpa.CakeEntity;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.NestedServletException;

public class CakeControllerTest extends AbstractTest {
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void getCakesList() throws Exception {
        String uri = "/getAllCakes";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        CakeEntity[] cakesList = super.mapFromJson(content, CakeEntity[].class);
        assertTrue(cakesList.length > 0);
    }

    @Test
    public void getCakes() throws Exception {
        String uri = "/cakes";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        CakeEntity[] cakesList = super.mapFromJson(content, CakeEntity[].class);
        assertTrue(cakesList.length > 0);
    }

    @Test
    public void addCake() throws Exception {
        String uri = "/cakes";
        CakeEntity birthday = new CakeEntity();
        birthday.setTitle("Anniversary Cake");
        birthday.setDescription("Anniversary Cake");
        birthday.setImage("anniversary.jpg");
        String inputJson = super.mapToJson(birthday);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
    }

    @Test
    public void addDuplicateCake() throws Exception {
        String uri = "/cakes";
        CakeEntity birthday = new CakeEntity();
        birthday.setTitle("Birthday cake");
        birthday.setDescription("Birthday");
        birthday.setImage("Birthday.jpg");
        String inputJson = super.mapToJson(birthday);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(409, status);
    }

    @Test(expected = org.springframework.web.util.NestedServletException.class)
    public void addCakeNoTitle_expectException() throws Exception {
        String uri = "/cakes";
        CakeEntity birthday = new CakeEntity();
        birthday.setTitle(null);
        birthday.setDescription("Birthday");
        birthday.setImage("Birthday.jpg");
        String inputJson = super.mapToJson(birthday);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        exceptionRule.expect(NestedServletException.class);
        exceptionRule.expectMessage("Cake title is required");

    }

    @Test(expected = org.springframework.web.util.NestedServletException.class)
    public void addCakeNoDescription_expectException() throws Exception {
        String uri = "/cakes";
        CakeEntity birthday = new CakeEntity();
        birthday.setTitle("Birthday");
        birthday.setDescription(null);
        birthday.setImage("Birthday.jpg");
        String inputJson = super.mapToJson(birthday);
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        exceptionRule.expect(NestedServletException.class);
        exceptionRule.expectMessage("Cake description is required");

    }

    @Test(expected = org.springframework.web.util.NestedServletException.class)
    public void addCakeNoImage_expectException() throws Exception {
        String uri = "/cakes";
        CakeEntity birthday = new CakeEntity();
        birthday.setTitle("Birthday");
        birthday.setDescription("test");
        birthday.setImage(null);
        String inputJson = super.mapToJson(birthday);
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();
        exceptionRule.expect(NestedServletException.class);
        exceptionRule.expectMessage("Cake image is required");

    }

}
