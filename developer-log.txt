Server Code Tasks
1) Create a new Spring Boot project from https://start.spring.io/ to hold the server code.
2) Create new packages: jpa, rest, and service to hold the micro-service code.
3) Add CakeEntity from the original Waracle Cake Manager project to the jpa package.
4) In the CakeEntity class refactor the column names to match JSON structure of cake.json
table name Employee > Cakes
employeeId variable > cakeId
@Column(name = "EMAIL" > @Column(name = "TITLE"
@Column(name = "FIRST_NAME" > @Column(name = "DESCRIPTION"
@Column(name = "LAST_NAME" > @Column(name = "IMAGE"
@UniqueConstraint(columnNames = "EMAIL") > @UniqueConstraint(columnNames = "TITLE")
5) Update resources > application.properties with the spring data source properties for the in-memory hsqldb DB.
6) In the jpa package create a new Spring jpa repository for the CakeEntity. Add a findByTitle method.
7) In CakeManagerApplication add an ApplicationRunner method to replicate the servlet functionality in the original Waracle Cake Manager project. This will read in the JSON file and save the cake entities.
8) Fix any compilation error in the above class and update the logging to use a valid logging framework rather than System.out.
9) After all compilation errors are resolved run CakeManagerApplicationTests class to check the JSON file upload process.
10) The above step fails due to duplicate titles in the retrieved JSON list so in the CakeManagerApplication class add a check to see if the title has already been added.
11) Once we have confirmed the CakeManagerApplication is correctly adding the new Cake Entities create a new Cake service interface class to get all cakes, add a cake, download the cakes list and validate if a cake title already exists.
12) Create a new Cake rest Controller to expose the endpoints for getting all cakes, adding a cake, downloading the cakes list and validating that the cake title has not already been added.
13) Look at validation for the Cake Entity and add any Not Blank or Max size annotations for the fields Title, Description and Image.
14) Add unit tests which will test the jpa, service, and rest code.
15) Once the unit tests are are working run the Spring boot application and hit each endpoint via postman. Check how we handle adding duplicate titles and missing attributes e.g. Title.
16) Enable CORS in the project to allow the Angular client to call each of the endpoints from localhost:4200 and the docker container port localhost:3000.
17) Once the Angular client adds support for OAuth2 in the client application we also need to make some supporting changes in the server code.


Client Code Tasks
1) Create a new Angular project to hold the client code using the Angular CLI.
2) Add Angular Material to this project.
3) Add a new Cakes List component which will display all the cakes currently in the system and have a download all cakes option.
4) Add a new Cakes Service that can be injected into any component which will allow the component to call the server REST endpoints.
5) Hook the Cakes Service up to the get all cakes, download cakes and add cake endpoints.
6) Update the Cakes List component to use the CakeService to fetch the cakes list and display on the front end.
7) Update the Cakes List component to use the CakeService to download the latest cakes list as JSON data.
8) Start up the Angular server and test the above.
9) This results in a CORS issue as we are calling from localhost:4200 > localhost:8080. Fix this in the server code and retest.
10) Add a new Cakes Add component which captures new cakes details and adds them to the server.
11) Add a handle error function to the Cakes Add component to deal with any errors e.g. duplicate title error being returned from the REST endpoint.
12) Fix the e2e tests that are failing as a basic starting point to allow the development of end to end integration tests using protractor.
13) Add support for the containerisation of the client by configuring the Angular application to run in a Docker container.
14) Integrate OAuth2 authentication into the Angular client by utilising the okta framework.