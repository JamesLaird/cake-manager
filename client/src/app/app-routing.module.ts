import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OktaAuthGuard } from '@okta/okta-angular';
import {CakesAddComponent} from './cakes-add/cakes-add.component';
import {CakesListComponent} from './cakes-list/cakes-list.component';

const routes: Routes = [
  { path: '', redirectTo: '/cakes-list', pathMatch: 'full', canActivate: [OktaAuthGuard] },
  {
    path: 'cakes-list',
    component: CakesListComponent,
    canActivate: [OktaAuthGuard]
  },
  {
    path: 'cakes-add',
    component: CakesAddComponent,
    canActivate: [OktaAuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
