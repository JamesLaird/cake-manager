import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MatButtonModule, MatCardModule, MatInputModule, MatListModule, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CakesListComponent } from './cakes-list/cakes-list.component';
import { CakesAddComponent } from './cakes-add/cakes-add.component';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AuthRoutingModule } from './auth-routing.module';

import { OktaAuthService, OktaAuthGuard } from '@okta/okta-angular';

@NgModule({
  declarations: [
    AppComponent,
    CakesListComponent,
    CakesAddComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    AppRoutingModule,
    HttpClientModule,
    FlashMessagesModule.forRoot(),
    AuthRoutingModule,
  ],
  providers: [OktaAuthService, OktaAuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule { }
