import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class CakesService {

  private API = '//localhost:8080';
  private CAKES_API = this.API + '/cakes';
  private ALL_CAKES_API = this.API + '/getAllCakes';

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
      return this.http.get(this.ALL_CAKES_API).pipe(
      catchError(this.handleError('getAll', []))
    );
  }

  downloadCakesList(): Observable<any> {
      return this.http.get(this.CAKES_API).pipe(
        catchError(this.handleError('getAll', []))
      );
  }

   addCake(cake: any): Observable<any> {
       const result = this.http.post(this.CAKES_API, cake);
       return result;
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console
      return of(result as T);
    };
  }
}
