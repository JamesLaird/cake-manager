import {async, TestBed} from '@angular/core/testing';

import { CakesService } from './cakes.service';
import {HttpClientModule} from '@angular/common/http';

describe('CakesService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
    })
      .compileComponents();
  }));

  it('should be created', () => {
    const service: CakesService = TestBed.get(CakesService);
    expect(service).toBeTruthy();
  });
});
