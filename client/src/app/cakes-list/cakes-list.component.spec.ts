import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CakesListComponent } from './cakes-list.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MatButtonModule, MatCardModule, MatInputModule, MatListModule, MatToolbarModule} from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';

describe('CakesListComponent', () => {
  let component: CakesListComponent;
  let fixture: ComponentFixture<CakesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CakesListComponent ],
      imports: [
        FormsModule,
        MatButtonModule,
        MatCardModule,
        MatInputModule,
        MatListModule,
        MatToolbarModule,
        RouterTestingModule,
        HttpClientModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CakesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
