import { Component, OnInit } from '@angular/core';
import {DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { CakesService } from '../shared/cakes/cakes.service';

@Component({
  selector: 'app-cakes-list',
  templateUrl: './cakes-list.component.html',
  styleUrls: ['./cakes-list.component.css']
})
export class CakesListComponent implements OnInit {

   cakes: Array<any>;
   downloadJsonHref: SafeUrl;

  constructor(private cakesService: CakesService, private sanitizer: DomSanitizer) { }

    ngOnInit() {
      try {
        this.downloadCakesList();
        this.cakesService.getAll().subscribe(data => {
          this.cakes = data;
        });
      } catch (e) {
      }
    }

    downloadCakesList() {
      this.cakesService.downloadCakesList().subscribe(response => {
        const theJSON = JSON.stringify(response);
        const uri = this.sanitizer.bypassSecurityTrustUrl('data:text/json;charset=UTF-8,' + encodeURIComponent(theJSON));
        this.downloadJsonHref = uri;
      });
    }
}
