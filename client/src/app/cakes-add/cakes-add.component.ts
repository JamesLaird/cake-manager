import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {CakesService} from '../shared/cakes/cakes.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-cakes-add',
  templateUrl: './cakes-add.component.html',
  styleUrls: ['./cakes-add.component.css']
})
export class CakesAddComponent implements OnInit {

  cake: any = {};

  constructor(private cakesService: CakesService, private flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
  }

  handleError(error) {
    const errorMessage = error;
    this.flashMessagesService.show(`${errorMessage.error.error}`, { cssClass: 'alert-danger', timeout: 5000 });
  }

  save(form: NgForm) {
    this.cakesService.addCake(form).subscribe(result => {
      this.flashMessagesService.show(`Cake ${this.cake.title} has been successfully added!`, { cssClass: 'alert-success', timeout: 5000 });
      form.reset();
    }, error => this.handleError(error));
  }

}
