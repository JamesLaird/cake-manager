import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CakesAddComponent } from './cakes-add.component';
import {FormsModule} from '@angular/forms';
import {MatButtonModule, MatCardModule, MatInputModule, MatListModule, MatToolbarModule} from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {FlashMessagesModule} from 'angular2-flash-messages';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('CakesAddComponent', () => {
  let component: CakesAddComponent;
  let fixture: ComponentFixture<CakesAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CakesAddComponent ],
      imports: [
        FormsModule,
        MatButtonModule,
        MatCardModule,
        MatInputModule,
        MatListModule,
        MatToolbarModule,
        RouterTestingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        FlashMessagesModule.forRoot(),
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CakesAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
