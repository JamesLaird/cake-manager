
# Angular Web Client
This is an Angular web application that provides a user interface to 

- list all cakes 

- add a new cake 

- download the current cake's list as a JSON file

## Prerequisites
This project has dependencies that require Node 10.9 or higher

The nvm ls and nvm use {nodeVersionNumber} commands can help configure your machine to be pointing to the correct Node version

- Valid Internet Browser

The current Angular v8 browser default support is for so-called "evergreen" browsers; the last versions of browsers that automatically update themselves. This includes Safari >= 10, Chrome >= 55 (including Opera), Edge >= 13 on the desktop, and iOS 10 and Chrome on mobile.

## Project Setup
This project was created via the Angular CLI in order to take advantage of its many developer friendly and performance tuning features. 
Please install this application via npm if you intend to run this code locally or create a production ready build.

- https://angular.io/guide/setup-local
- https://github.com/angular/angular-cli
- https://angular.io/docs/ts/latest/cli-quickstart.html

You can verify that the Angular CLI was successfully installed by running this command `ng -v` which will display the current Angular CLI version.

## Project Build
After the Angular CLI has been installed successfully, from a command prompt or terminal window go to the project client folder, then type the following cmd `npm install` to build the required node_modules:
	
## Authentication via OAuth2

This application provides Authentication via OAuth2 using the okta framework:

https://developer.okta.com/code/angular/

In order to enable this to work with the Angular client you must:

- Log in to your Okta Developer account and navigate to Applications > Add Application. Click Single-Page App, click Next, and give the app a name you will remember. 
   Change all instances of http://localhost:8080 to http://localhost:4200 and click Done.

- You will see a client ID at the bottom of the page. Add it and an issuer property to src/main/resources/application.properties in the server project
   
   okta.oauth2.client-id={yourOktaClientId}
   
   okta.oauth2.issuer=https://{yourOktaDomain}/oauth2/default
  
   NOTE: The value of {yourOktaDomain} should be something like dev-123456.okta.com. Ensure you do not include -admin in the value!

  For the client configuration, set the issuer and copy the clientId into the oktaConfig constant in client/src/app/auth-routing.module.ts
  
  const oktaConfig = {
    issuer: 'https://{yourOktaDomain}/oauth2/default',
    redirectUri: window.location.origin + '/implicit/callback',
    clientId: '{oktaClientId}'
  };

## Local Development Server

From a command prompt or terminal window go to the project client folder and type `ng serve` to launch a local a dev server. Navigate to `http://localhost:4200/` where you will be asked to log-in to you okta account. The app will automatically reload if you change any of the source files.

## Production Build

From a command prompt or terminal window go to the project client folder and type `ng build --prod` to build the project for a production ready release. The build artifacts will be stored in the `dist/` directory which can then be deployed to the server of your choice.  

Using the `--prod` flag will take advantage of the Angular CLI the Ahead-of-Time (AOT) Compiler feature:
https://angular.io/guide/aot-compiler

The `--prod` flag will also make use of different optimisation techniques including bundling, tree-shaking, and run dead code elimination via UglifyJS.

## Running Unit Tests

From a command prompt or terminal window go to the project client folder and type `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end Tests

From a command prompt or terminal window go to the project client folder and type `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Containerisation

As well as running on the local development server this app has been configured to run on a docker container. 

As we are using OAuth2 authentication you must first of all log in to your Okta Developer account and change all instances of http://localhost:4200 to http://localhost:3000

In order to build the docker image we must follow the following steps:

- We need the latest production build in the dist folder (follow the production build steps above)

- From a command prompt or terminal window go to the project client folder and type `docker image build -t client`

- From a command prompt or terminal window go to the project client folder and type `docker image ls`

- From a command prompt or terminal window go to the project client folder and type `docker run -p 3000:80 --rm client`

If you now navigate to the `http://localhost:3000` the Angular application will be up and running there as opposed to `http://localhost:4200`

## Code Quality Check

From a command prompt or terminal window go to the project client folder and type `ng lint`. This will highlight any linting errors that require resolved.

## User Testing Notes

* To test duplicate cake values when adding a cake enter 'Lemon cheesecake' as the cake title. This title has already been imported by the JSON upload.



