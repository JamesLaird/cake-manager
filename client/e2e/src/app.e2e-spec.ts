import { AppPage } from './app.po';

describe('Waracle Client App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Welcome to Waracle Cake Manager!');
  });

});
