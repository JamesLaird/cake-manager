## Waracle Cake Manager Application

This is an Angular web application that provides a user interface to 

- list all cakes 

- add a new cake 

- download the current cake's list as a JSON file

It makes REST calls to a Cake Manager Spring Boot service.

## Running Spring Boot Service

### Prerequisites

- Java 8
- Maven
- Node

### Loading into IntelliJ
From the main IntelliJ startup menu - Import Project > Import project from external model > Maven

or from an opened IntelliJ - Top Menu > File > New > Project from Existing Sources (select directory) > Import project from external model > Maven

### API Docs
The rest API docs can be viewed here:

http://localhost:8080/swagger-ui/index.html

### Logging
The Logging framework used in this code is SLF4J. The debug logging is output at INFO level.

This can be changed in the application.properties if required.

### Usage
- This Spring Boot application is configured to listen on port 8080
- The tomcat logs directory is /tmp/tomcat/cake-manager

### Authentication

This application provides Authentication via OAuth2 using the okta framework:

https://developer.okta.com/code/angular/

You must add your okta client id and an issuer property to src/main/resources/application.properties
            
okta.oauth2.client-id={yourClientId}

okta.oauth2.issuer=https://{yourOktaDomain}/oauth2/default

See the README file in the root of the client directory for more details of how to set-up you okta account.

### Maven commands (from a command prompt or IntelliJ terminal window)

If running from the command prompt or terminal, go to the project's root folder, then type:

./mvnw clean verify  						(to run unit and integration tests)

./mvnw clean package 						(to build the application)
    
./mvnw spring-boot:run 						(to start the application)

## Angular Web Client
See the README file in the root of the client directory for how the Angular app gets built and deployed
